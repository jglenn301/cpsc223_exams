#include <stdio.h>
#include <ctype.h>
#include <string.h>

void count_spaces(char *s, int *leading, int *trailing);

int main(int argc, char **argv)
{
  if (argc > 1)
    {
      int leading = 0, trailing = 0;
      
      count_spaces(argv[1], &leading, &trailing);
      
      printf("leading=%d trailing=%d\n", leading, trailing);
    }
}

void count_spaces(char *s, int *leading, int *trailing)
{
  int len = strlen(s);
  
  char *start = s;
  while (isspace(*start))
    {
      start++;
    }
  *leading = start - s;
  
  char *end = s + len;
  while (end > s && isspace(*(end - 1)))
    {
      end--;
    }
  *trailing = len - (end - s);
}

