#include <stdio.h>
#include <stdlib.h>

struct point { int x; int y; };

void part123();
void part4();

int main()
{
  part123();
}

void part123()
{
  char a[] = "XNA";
  
  char *b = a;

  char **c = &b;
  
  struct point *d = malloc(sizeof(struct point));
  d->x = 1;
  d->y = 2;

  int *e = malloc(2 * sizeof(int));
  e[0] = 0;
  e[1] = 1;

  int *f[2];
  f[0] = e + 1;
  f[1] = malloc(sizeof(int));
  *f[1] = -1;

  // PROBLEM 3 (commented out are invalid)
  (*b)++;
  printf("%s\n", a); // YNA
  
  b++;
  printf("%c\n", *b); // N

  // warning: incompatible pointer types (&a pointer to an array of 4 chars, c is pointer to pointer to char)
  //c = &a;
  //printf("%c\n", c[0]);

  // warning: conversion from pointer to int
  //d->x = f[1];
  //printf("%d\n", d->x);
  
  *f = e;
  printf("%d\n", *f[0]); // 0
  
  f[1]--;
  printf("%d\n", *f[1]); // undefined

  // PROBLEM 4
  f[0] = &d->y;
  e[0] = *f[1];

  // PROBLEM 2
  free(d);
  free(e);
  free(f[1]);


  // just to avoid unused variable warning
  c = c;
}
