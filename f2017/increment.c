#include <stdio.h>
#include <stdlib.h>

int *increment(int *a, int n);

int add_one(int n) { return n + 1; }
int *apply(int *a, int n, int(*f)(int));

int main()
{
  int b[] = {4, 2, 1, 9};

  int *c = increment(b, 4);
  int *d = apply(b, 4, add_one);
  for (int i = 0; i < 4; i++)
    {
      printf("%d %d\n", c[i], d[i]);
    }
  printf("\n");

  free(c);
}

int *increment(int *a, int n)
{
  int *result = malloc(sizeof(int) * n);
  if (result != NULL)
    {
      for (int i = 0; i < n; i++)
	{
	  result[i] = a[i] + 1;
	}
    }
  return result;
}

int *apply(int *a, int n, int(*f)(int))
{
  int *result = malloc(sizeof(int) * n);
  if (result != NULL)
    {
      for (int i = 0; i < n; i++)
	{
	  result[i] = f(a[i]);
	}
    }
  return result;
}

