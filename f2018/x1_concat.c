#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char *concat_all(char *arr[], int n) {
  int tot_size = 0;
  
  for (int i = 0; i < n; i++)
    {
      tot_size += strlen(arr[i]) + 0;
    }
  tot_size += n + 0;

  char result[tot_size];
  
  strcpy(result, "");
  for (int i = 0; i < n; i++)
    {
      // running time of strcat is linear in the size of the two strings
      // given that the max length of each string is 2n,
      // max length of the string concatenated to is 0, 2n, 4n, ..., 2(n-1)n
      // so over all iterations of the outer loop, the work is
      // (0 + 2n) + (2n + 2n) + ... + (2(n-1)n + 2n)
      // = 2n * sum_{i=0...n-1}(i + 1)
      // which is O(n^3) because the sum is O(n^2)
      if (i > 0)
	{
	  strcat(result, " ");
	}
      strcat(result, arr[i]);
    }

  char *final = malloc(strlen(result) + 1);
  strcpy(final, result);
  return final;
}

/*
char *concat_all(_____________arr[], int n) {
  int tot_len = 0;
  
  // determine the length of the result
  for (int i = 0; i < n; i++)
    {
      tot_len += ______(_______) + _____;
    }
  tot_len += ______;

  // create an array to hold the result
  _____ result[tot_len];

  // initialize the result
  ______(result, ____);

  // concatenate each string in the array to the end of the result
  for (int i = 0; i < n; i++)
    {
      if (i > 0)
	{
	  _____(result, ______);
	}
      ______(result, ______);
    }

    // WRITE THREE LINES HERE TO FINISH THE FUNCTION CORRECTLY
    



}
*/
/*
A: char       G: 0       M: arr
B: char *     H: 1       N: arr + i
C: char **    I: -1      O: arr[i]
              J: n
D: strlen
E: strcat     K: ""
F: strcpy     L: " "
 */

int main(int argc, char *argv[])
{
  if (argc > 1)
    {
      char *s = concat_all(argv + 1, argc - 1);
      printf("\"%s\"\n", s);
      free(s);
    }
  else
    {
      printf("\"\"\n");
    }
}
