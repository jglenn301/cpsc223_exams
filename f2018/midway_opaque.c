#include <stdio.h>
#include <stdlib.h>
#include "location_opaque.h"

int main() {
  double lat1, lon1, lat2, lon2;
  lat1 = 47.6;
  lon1 = -122.3;
  lat2 = 38.9;
  lon2 = -77.0;

  // INITIALIZATIONS AND ERROR HANDLING AND OMITTED FOR BREVITY
  
  location *hq1 = location_create(lat1, lon1);
  location *hq2 = location_create(lat2, lon2);
  location *mid = location_create(0.0, 0.0);

  location_midway(hq1, hq2, mid);

  location_print(mid, stdout);

  location_destroy(hq1);
  location_destroy(hq2);
  location_destroy(mid);
}
