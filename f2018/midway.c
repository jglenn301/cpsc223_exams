#include <stdio.h>
#include <stdlib.h>
#include "location.h"

int main() {
  double lat1, lon1, lat2, lon2;
  lat1 = 47.6;
  lon1 = -122.3;
  lat2 = 38.9;
  lon2 = -77.0;

  // INITIALIZATIONS AND ERROR HANDLING AND OMITTED FOR BREVITY
  
  location hq1 = {lat1, lon1};
  location hq2 = {lat2, lon2};
  location mid;

  location_midway(&hq1, &hq2, &mid);

  printf("%f %f\n", mid.lat, mid.lon);
}
