#ifndef __LOCATION_H__
#define __LOCATION_H__

typedef struct location location;

// copies to the struct pointed to by mid the point halfway between from and to
void location_midway(const location *from, const location *to, location *mid);

location *location_create(double lat, double lon);
void location_print(const location *loc, FILE * out);
void location_destroy(location *loc);


#endif
