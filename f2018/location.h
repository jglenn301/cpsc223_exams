#ifndef __LOCATION_H__
#define __LOCATION_H__

struct location { double lat; double lon; };

typedef struct location location;

// copies to the struct pointed to by mid the point halfway between from and to
void location_midway(const location *from, const location *to, location *mid);

#endif
