#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "location_opaque.h"

struct location { double lat; double lon; };

location *location_create(double lat, double lon)
{
  location *loc = malloc(sizeof(location));
  loc->lat = lat;
  loc->lon = lon;
  return loc;
}

void location_destroy(location *loc)
{
  free(loc);
}

void location_print(const location *loc, FILE * out)
{
  fprintf(out, "%f %f\n", loc->lat, loc->lon);
}

void location_midway(const location *from, const location *to, location *mid) {
  // HEAVY-DUTY MATH OMITTED FOR BREVITY
  mid->lat = (from->lat + to->lat) / 2;
  mid->lon = (from->lon + to->lon) / 2;
}
