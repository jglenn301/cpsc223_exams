#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "smap.h"
#include "sort.h"

void check_membership(const char *key, int *value, void *arg1, void *arg2)
{
  smap *m2 = arg1;
  int *count = arg2;

  if (smap_contains_key(m2, key))
    {
      (*count)++;
    }
}

bool same_keys(smap *m1, smap *m2)
{
  if (m1 == NULL || m2 == NULL || smap_size(m1) != smap_size(m2))
    {
      return false;
    }

  int match_count = 0;
  smap_for_each(m1, check_membership, m2, &match_count);
  return match_count == smap_size(m1);
}

int compare_keys(const void *k1, const void *k2)
{
  const char * const *s1 = k1;
  const char * const *s2 = k2;

  return strcmp(*s1, *s2);
}

void add_to_array(const char *key, int *value, void *arg1, void *arg2)
{
  const char **arr = arg1;
  int *count = arg2;
  arr[*count] = key;
  (*count)++;
}

bool sorted(const char **arr, int n)
{
  int i = 1;
  while (i < n && strcmp(arr[i - 1], arr[i]) < 0)
    {
      i++;
    }
  return i == n;
}

bool same_keys_alt(smap *m1, smap *m2)
{
  if (m1 == NULL || m2 == NULL || smap_size(m1) != smap_size(m2))
    {
      return false;
    }

  const char **a1 = malloc(sizeof(const char *) * smap_size(m1));
  const char **a2 = malloc(sizeof(const char *) * smap_size(m2));
  
  int count1 = 0;
  int count2 = 0;
  smap_for_each(m1, add_to_array, a1, &count1);
  smap_for_each(m2, add_to_array, a2, &count2);

  if (!sorted(a1, count1))
    {
      mergesort(a1, count1, sizeof(char *), compare_keys);
    }
  if (!sorted(a2, count2))
    {
      mergesort(a2, count2, sizeof(char *), compare_keys);
    }

  int i = 0;
  while (i < smap_size(m1) && strcmp(a1[i], a2[i]) == 0)
    {
      i++;
    }
  return (i == smap_size(m1));
}

int main(int argc, char **argv)
{
  smap *m1 = smap_create(smap_default_hash);
  smap_put(m1, "PAE", NULL);
  smap_put(m1, "SEA", NULL);
  smap_put(m1, "BWI", NULL);
  
  smap *m2 = smap_create(smap_default_hash);
  smap_put(m2, "SEA", NULL);
  smap_put(m2, "BWI", NULL);
  smap_put(m2, "PAE", NULL);

  smap *m3 = smap_create(smap_default_hash);
  smap_put(m3, "SEA", NULL);
  smap_put(m3, "DCA", NULL);
  smap_put(m3, "PAE", NULL);

  printf("%d\n", same_keys(m1, m2));
  printf("%d\n", same_keys(m1, m3));
  printf("%d\n", same_keys_alt(m1, m2));
  printf("%d\n", same_keys_alt(m1, m3));
}

  
