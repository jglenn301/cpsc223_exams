#include <stdio.h>
#include <stdlib.h>

typedef struct location
{
  double lat;
  double lon;
} location;

void setup()
{
  char a[] = "MH";
  char *b = a + 1;
  location c = {45.1, 78.4};
  double * d = malloc(sizeof(double));
  *d = 8.91;
  location *e = malloc(sizeof(location) * 2);
  e[0] = c;
  e[1] = c;
  double *f[] = {d, &e[1].lon};

  // silence unused variable warnings
  b = b;
  f[0] = f[0];
}

void freeup()
{
  char a[] = "MH";
  char *b = a + 1;
  location c = {45.1, 78.4};
  double * d = malloc(sizeof(double));
  *d = 8.91;
  location *e = malloc(sizeof(location) * 2);
  e[0] = c;
  e[1] = c;
  double *f[] = {d, &e[1].lon};

  free(d);
  free(e);

  // silence unused variable warnings
  b = b;
  f[0] = f[0];
}

void execute()
{
  char a[] = "MH";
  char *b = a + 1;
  location c = {45.1, 78.4};
  double * d = malloc(sizeof(double));
  *d = 8.91;
  location *e = malloc(sizeof(location) * 2);
  e[0] = c;
  e[1] = c;
  double *f[] = {d, &e[1].lon};

  // uncomment the pairs below one at a time to execute
  
  //(*b)++;
  //printf("%s\n", a); // MI
   
  //b--;
  //printf("%s\n", b); // MH
  
  /*
    d = &f;
    printf("%f\n", *d);
  */
  
  //d = f[1];
  //printf("%f\n", *d); // 78.40000
  
  /*
    f = &d;
    printf("%d\n", *f[0]);
  */
  
  //c = e[2];
  //printf("%f\n", c.lat); // undefined
  
  //d--;
  //printf("%f\n", d[1]); // undefined (ptr arithmetic defined *within* array or one past last elt; see 6.5.6 #8)

  // silence unused variable warnings
  a[0] = a[0];
  b = b;
  c = c;
  d = d;
  e = e;
  f[0] = f[0];
}

void modify()
{
  char a[] = "MH";
  char *b = a + 1;
  location c = {45.1, 78.4};
  double * d = malloc(sizeof(double));
  *d = 8.91;
  location *e = malloc(sizeof(location) * 2);
  e[0] = c;
  e[1] = c;
  double *f[] = {d, &e[1].lon};

  *d = e[1].lon;
  c = e[0];
  e = &c;
  *f[1] = 10.0;

  // silence unused variable warnings
  b = b;
}

int main(int argc, char *argv[])
{
  setup();
  freeup();
  execute();
  modify();

  return 1;
}
