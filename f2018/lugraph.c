#define _GNU_SOURCE

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include "lugraph.h"

lu_search *lugraph_connected_components(const lugraph *g)
{
  lu_search *s = lu_search_create(g->n);
  
  for (int i = 0; i < g->n; i++)
    {
      if (s->status[i] == UNSEEN)
	{
	  ldigraph_comp_visit(g, s, i);
	  s->num++;
	}
    }
  return s;
}

void lugraph_dfs_visit(const lugraph* g, lu_search *s, int from)
{
  s->status[from] = PROCESSING;
  
  // iterate over outgoing edges
  for (int i = 0; i < g->list_size[from]; i++)
    {
      int to = g->adj[from][i];
      if (s->status[to] == UNSEEN)
	{
	  // found an edge to a new vertex -- explore it
	  lugraph_dfs_visit(g, s, to);
	}
    }
  
  // record current vertex finished
  s->status[from] = DONE;
  s->vertices[s->num][s->size[s->num]] = from;
  s->size[s->num]++;
}
