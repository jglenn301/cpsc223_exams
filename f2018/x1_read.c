#include <stdio.h>
#include <stdlib.h>

double normalize(double x)
{
  x = x - 360.0 * (int)(x / 360.0);
  return x >= 0.0 ? x : x + 360.0;
}

double *read(int n)
{
  double *values = malloc(sizeof(double) * n);

  for (int i = 0; i < n; i++)
    {
      double x;
      scanf("%lf", &x);
      values[i] = normalize(x);
    }

  return values;
}

double *read_fixed(int n, int *count, const char *fname, double (*f)(double))
{
  *count = 0;
  FILE *in = fopen(fname, "r");
  if (in == NULL)
    {
      *count = 0;
      return NULL;
    }

  double *values = malloc(sizeof(double) * n);
  if (values == NULL)
    {
      *count = 0;
      fclose(in);
      return values;
    }

  for (*count = 0; *count < n; (*count)++)
    {
      double x;
      if (fscanf(in, "%lf", &x) < 1)
	{
	  fclose(in);
	  return values;
	}
      values[*count] = f(x);
    }

  fclose(in);
  return values;
}

int main(int argc, char **argv)
{
  int size = 21;
  int count;
  //double *arr = read(size);
  double *arr = read_fixed(size, &count, "infile", normalize);
  for (int i = 0; i < count; i++)
    {
      printf("%f\n", arr[i]);
    }
  free(arr);
}
