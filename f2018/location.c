#include <math.h>
#include "location.h"

void location_midway(const location *from, const location *to, location *mid) {
  // HEAVY-DUTY MATH OMITTED FOR BREVITY
  mid->lat = (from->lat + to->lat) / 2;
  mid->lon = (from->lon + to->lon) / 2;
}
