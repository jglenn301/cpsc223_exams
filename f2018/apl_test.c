#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "apl.h"

bool lists_equal(const apl* l1, const apl* l2)
{
  if (l1 == NULL || l2 == NULL || apl_size(l1) != apl_size(l2))
    {
      return false;
    }
  int i = 0;
  while (i < apl_size(l1) && strcmp(apl_get(l1, i), apl_get(l2, i)) == 0)
    {
      i++;
    }
  return i == apl_size(l1);
}

bool lists_equal_it(const apl* l1, const apl* l2)
{
  if (l1 == NULL || l2 == NULL || apl_size(l1) != apl_size(l2))
    {
      return false;
    }

  apl_it *i = apl_start(l1);
  apl_it *j = apl_start(l2);
  const char *s1;
  const char *s2;
  while ((s1 = apl_it_get(i)) != NULL
	 && (s2 = apl_it_get(j)) != NULL
	 && strcmp(s1, s2) == 0);
  return s1 == NULL;
}

int main(int argc, char **argv)
{
  apl *l1 = apl_create();
  apl_add(l1, "PAE");
  apl_add(l1, "SEA");
  apl_add(l1, "KEH");
  
  apl *l2 = apl_create();
  apl_add(l2, "PAE");
  apl_add(l2, "SEA");
  apl_add(l2, "KEH");

  apl *l3 = apl_create();
  apl_add(l3, "KEH");
  apl_add(l3, "PAE");
  apl_add(l3, "SEA");

  printf("%d\n", lists_equal(l1, l2));
  printf("%d\n", lists_equal(l1, l3));
  printf("%d\n", lists_equal_it(l1, l2));
  printf("%d\n", lists_equal_it(l1, l3));
}
