#include "tour.h"

#include <stdlib.h>
#include <string.h>

struct tour
{
  city **fragments;
  int *frag_len;
  int frag_count;
  int frag_capacity;
};

#define INITIAL_CAPACITY 1

void tour_embiggen(tour *t);

tour *tour_init(const char *c1, double lat1, double lon1, const char *c2, double lat2, double lon2)
{
  // check that city names are three characters
  if (strlen(c1) != 3 || strlen(c2) != 3)
    {
      return NULL;
    }

  tour *t = malloc(sizeof(tour));
  t->fragments = malloc(sizeof(city *) * INITIAL_CAPACITY);
  t->frag_len = malloc(sizeof(int) * INITIAL_CAPACITY);
  
  t->frag_count = 1;
  t->frag_capacity = INITIAL_CAPACITY;

  t->frag_len[0] = 2;
  t->fragments[0] = malloc(sizeof(city) * 2);
  
  strcpy(t->fragments[0][0].name, c1);
  t->fragments[0][0].loc.lat = lat1;
  t->fragments[0][0].loc.lon = lon1;

  strcpy(t->fragments[0][1].name, c2);
  t->fragments[0][1].loc.lat = lat2;
  t->fragments[0][1].loc.lon = lon2;

  return t;
}

void tour_destroy(tour *t)
{
  // free each fragment
  for (int i = 0; i < t->frag_count; i++)
    {
      free(t->fragments[i]);
    }
  
  // free the array of pointers to fragments
  free(t->fragments);

  // free the array of fragment lengths
  free(t->frag_len); // can be before for loop

  // free the metadata struct
  free(t);
}

void tour_for_each(tour *t, void (*process_city)(const city *), void (*between_fragments)())
{
  // loop over each fragment...
  for (int i = 0; i < t->frag_count; i++)
    {
      // ...and each city within each fragment
      for (int j = 0; j < t->frag_len[i]; j++)
	{
	  // pass the city to the processing function
	  process_city(t->fragments[i] + j);
	}
      
      if (i < t->frag_count - 1)
	{
	  between_fragments();
	}
    }
}
