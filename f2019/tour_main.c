#include <stdio.h>

#include "tour.h"

void print_city(const city *c);
void print_newline();

int main(int argc, char **argv)
{
  tour *t = tour_init("ACC", 5.6, -0.2, "LOS", 6.6, 3.3);

  tour_for_each(t, print_city, print_newline);
  
  tour_destroy(t);
  
  return 0;
}

void print_city(const city *c)
{
  printf("%s %f %f\n", c->name, c->loc.lat, c->loc.lon);
}

void print_newline()
{
  printf("\n");
}

