#ifndef __TOUR_H__
#define __TOUR_H__

typedef struct location
{
  double lat;
  double lon;
} location;


typedef struct city {
  char name[4];
  location loc;
} city;

typedef struct tour tour;

/**
 * Creates a tour with one fragment containing the two given cities.
 *
 * @param c1 a 3-character string
 * @param lat1 a double between -90 and 90
 * @param lon1 a double between -180 and 180
 * @param c2 a 3-character string
 * @param lat2 a double between -90 and 90
 * @param lon2 a double between -180 and 180
 * @return a pointer to the newly created tour
 */
tour *tour_init(const char *c1, double lat1, double lon1, const char *c2, double lat2, double lon2);

/**
 * Destroys the given tour.
 *
 * @param t a pointer to a tour
 */
void tour_destroy(tour *t);

/**
 * Passes each city in the given tour to the given function, calling
 * the second function between each fragment.
 *
 * @param t a pointer to a tour
 * @param process_city a function that takes a pointer to a city
 * @param between_fragments a function
 */
void tour_for_each(tour *t, void (*process_city)(const city *), void (*between_fragments)());

#endif
