#include <stdio.h>
#include <math.h>

double distance(double x, double y);
double sum_distance(char *fname);
double sum_whatever(char *fname, double(*)(double, double));

int main(int argc, char **argv)
{
  printf("%f\n", sum_distance("infile"));
  printf("%f\n", sum_whatever("infile", distance));
}

double distance(double x, double y)
{
  return sqrt(x * x + y * y);
}

double sum_distance(char *fname)
{
  double total = 0.0;
  FILE *in = fopen(fname, "r");
  if (in)
    {
      double x, y;
      while (fscanf(in, "%lf %lf", &x, &y) == 2)
	{
	  total += distance(x, y);
	}
      fclose(in);
    }
  
  return total;
}

double sum_whatever(char *fname, double (*whatever)(double, double))
{
  double total = 0.0;
  FILE *in = fopen(fname, "r");
  if (in)
    {
      double x, y;
      while (fscanf(in, "%lf %lf", &x, &y) == 2)
	{
	  total += whatever(x, y);
	}
      fclose(in);
    }
  
  return total;
}
