#include <stdio.h>
#include <stdlib.h>

typedef struct point
{
  int x;
  int y;
} point;

int main(int argc, char **argv)
{
  char a[4] = "CTU";
  char *b = a;
  point c[] = {{1, 2}, {4, 3}}; // or point c[2]; c[0].x = 1; ...
  point *d = c;                 // or point (*d)[2]
  int **e = malloc(sizeof(int *) * 2);
  e[0] = &c[0].y;               // or e[0] = &c->y;
  e[1] = malloc(sizeof(int));
  *e[1] = -1;                 // or e[1][0] = -1;

  (*a)++;
  printf("%s\n", a); // DTU
  
  b++;
  printf("%s\n", b); // TU
  
  //c[0].x = e[1];   // invalid
  //printf("%d\n", c[0].x);
  
  d->x = d[1].y;     
  printf("%d\n", d->x); // 3
  
  e[0]++;         
  printf("%d\n", *e[0]); // undefined
  
  (*e[1])++;
  printf("%d\n", *e[1]); // 0

  free(e[1]);
  free(e);
}
