#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void repeat_string(char *dest, int n, const char *src);

int main(int argc, char **argv)
{
  if (argc < 3 || strlen(argv[2]) == 0)
    {
      fprintf(stderr, "USAGE: %s n source-string\n", argv[0]);
      return 0;
    }
  
  int n = atoi(argv[1]);

  if (n > 0)
    {
      char dest[n];
      
      repeat_string(dest, n, argv[2]);
      printf("%s\n", dest);
    }
}

void repeat_string(char *dest, int n, const char *src)
{
  if (dest == NULL || src == NULL || n <= 0)
    {
      return;
    }

  // make first copy -- copy until end of src or until n-1 chars copied
  int i = 0;
  for (const char *s = src; i < n - 1 && *s != '\0'; i++, s++)
    {
      dest[i] = *s;
    }
  int len = i;

  // make more copies -- each subsequent character is the same as the one
  // len before it
  for (; i < n - 1; i++)
    {
      dest[i] = dest[i - len];
    }

  // insert the trailing null character
  dest[n - 1] = '\0';
}

